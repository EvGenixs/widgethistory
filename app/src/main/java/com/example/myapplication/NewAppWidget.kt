package com.example.myapplication

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import android.view.OnReceiveContentListener
import android.widget.RemoteViews
import android.widget.Toast
import com.example.myapplication.NewAppWidget.Companion.click

/**
 * Implementation of App Widget functionality.
 */
class NewAppWidget : AppWidgetProvider() {
    companion object{
        val click = "com.example.myapplication.CLICK"
        internal fun updateAppWidget(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int){
            var onClickIntent = Intent(context, NewAppWidget::class.java)
            onClickIntent.action = click
            onClickIntent.putExtra("appWidgetId", appWidgetId)
            val pendingIntent: PendingIntent = PendingIntent.getBroadcast(context, 0, onClickIntent, 0)
            val views = RemoteViews(
                context.packageName,
                R.layout.new_app_widget
            )
            views.setOnClickPendingIntent(R.id.appwidget_text, pendingIntent)
            appWidgetManager.updateAppWidget(appWidgetId,views)
        }
    }
    override fun onReceive(context: Context?, intent: Intent?) {
        if(click == intent?.action){
            Toast.makeText(context, "ura naxyu", Toast.LENGTH_SHORT).show()
        }
        super.onReceive(context, intent)
    }

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }

    }

    override fun onEnabled(context: Context) {
        var onClickIntent = Intent(context, NewAppWidget::class.java)
        onClickIntent.action = click
        val pendingIntent: PendingIntent = PendingIntent.getBroadcast(context, 0, onClickIntent, 0)
        val views = RemoteViews(
            context.packageName,
            R.layout.new_app_widget
        )
        views.setOnClickPendingIntent(R.id.appwidget_text, pendingIntent)
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

//internal fun updateAppWidget(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int) {
//    var time = 30
//    val views = RemoteViews(context.packageName, R.layout.new_app_widget)
//    var timer = object: CountDownTimer(30000,1000){
//        override fun onTick(p0: Long) {
//            views.setTextViewText(R.id.appwidget_text, time.toString())
//            appWidgetManager.updateAppWidget(appWidgetId, views)
//            time--
//        }
//        override fun onFinish() {
//
//        }
//
//    }
//    timer.start()
//
//    // Construct the RemoteViews object
//    // Instruct the widget manager to update the widget
//
//}